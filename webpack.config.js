const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


module.exports = {
  'mode': 'development',
  'entry': path.resolve(__dirname, 'src', 'app.js'),
  'devServer': {
    'contentBase': './www',
    'open': false
  },
  'output': {
    'path': path.resolve(__dirname, 'www'),
    'filename': 'bundle.[id].js'
  },
  'plugins': [

    //Clean folder
    new CleanWebpackPlugin(['dist']),

    //Extract sass
    new MiniCssExtractPlugin({
      'filename': '[name].css',
      'chunkFilename': '[id].css'
    }),

    //Index page
    new HtmlWebpackPlugin({
      'template': path.resolve(__dirname, 'src', 'www', 'index.html'),
      'inject': true,
      'filename': 'index.html'
    }),

  ],
  'module': {
    'rules': [
      {
        'test': /\.(png|svg|jpg|jpeg|gif)$/,
        'use': [
          {
            'loader': 'url-loader',
            'options': {
              'limit': 8000,
              'name': 'images/[hash]-[name].[ext]'
            }
          }

        ]
      },
      {
        'test': /\.(html)$/,
        'use': {
          'loader': 'html-loader',
          'options': {
            'interpolate': true,
          }
        }
      },
      {
        'test': /\.(sass|scss)$/,
        'use': [
          {'loader': true ? 'style-loader' : MiniCssExtractPlugin.loader},
          {'loader': 'css-loader', 'options': {'sourceMap': true} },
          {
            'loader': 'postcss-loader',
            'options': {
              'sourceMap': true,
              'plugins': function() { return [require('precss'), require('autoprefixer')]; }
            }
          },
          {'loader': 'resolve-url-loader'},
          {'loader': 'sass-loader'}
        ]
      },
    ]
  }
};
