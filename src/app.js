import './scss/main.scss';
import 'bootstrap';

import './app/AesomeImageSwitcher/loader';
import CardHolder from './app/cards/CardHolder';
import ScrollTrigger from './app/scrollTrigger/ScrollTrigger';

import hoverPlugin from 'hover-js';
import jQuery from 'jquery';

document.addEventListener('DOMContentLoaded', () => {
  hoverPlugin.start();

  ScrollTrigger.start();

  _initCardHolder();
});


/***
 * Init card holder
 * @private
 */
function _initCardHolder() {

  let cardHolder = document.querySelectorAll('#technologies .card-holder');
  if(!cardHolder.length) { return; }

  let cardHolderInstance = new CardHolder(cardHolder[0]);
  let anchors = document.querySelectorAll('#technologies a');

  anchors.forEach(anchor => {
    anchor.addEventListener('click', (evt) => {
      evt.preventDefault();
      evt.stopPropagation();

      try {

        let currentActiveLi = document.querySelectorAll('#technologies .circle-container li');

        for (let node of currentActiveLi) {
          node.classList.remove('active');
        }

        let anchorTarget = anchor.dataset.target;
        cardHolderInstance.activate(anchorTarget);

        anchor.parentElement.classList.add('active');
      } catch(error) {
      }
    });
  });
}
