const DOM_QUERYSELECTOR = '[data-scroll-trigger][data-scroll-trigger-target]';

class ScrollTriggerItem {

  constructor(element) {
    this.element = element;
    this.isActive = false;
  }


  shouldActivate() {
    let ebr = this.element.getBoundingClientRect();

    return ebr.top <= 0 && ebr.top + ebr.height >= 0;
  }

  /**
   * Activate it
   */
  activate() {

    //It's already active? don't do nothing
    if(this.isActive) { return; }

    this.isActive = true;

    let target = this.element.dataset.scrollTriggerTarget;
    let className = this.element.dataset.scrollTrigger;
    let targets = document.querySelectorAll(target);

    for(let target of targets) {
      target.classList.add(className);
    }
  }

  /**
   * Deactivate it
   */
  deactivate() {

    //It's not already active? don't do nothing
    if(!this.isActive) { return; }

    this.isActive = false;

    let target = this.element.dataset.scrollTriggerTarget;
    let className = this.element.dataset.scrollTrigger;
    let targets = document.querySelectorAll(target);

    for(let target of targets) {
      target.classList.remove(className);
    }
  }

}

class ScrollTrigger {

  constructor() {
    this.scrollHandler = null;
    this.items = [];


  }

  /**
   * Start ScrollTrigger
   */
  start() {

    _fullFillItems.apply(this);
    _deactivateItems.apply(this);
    _activateItems.apply(this);

    this.scrollHandler = evt => {

      //First all, fullfill all elements
      _fullFillItems.apply(this);

      _deactivateItems.apply(this);
      _activateItems.apply(this);
    };
    document.addEventListener('scroll', this.scrollHandler);
  }

  stop() {
    document.removeEventListener('scroll', this.scrollHandler);
  }
}

/**
 * Activate items that should be active
 * @private
 */
function _activateItems() {
  let shouldActivate = this.items.filter(x => !x.isActive && x.shouldActivate());

  //For each item that should activate
  for(let item of shouldActivate) {
    item.activate();
  }
}

/**
 * Deactivate items that shouldn't be still active
 * @private
 */
function _deactivateItems() {
  let shouldDeactivate = this.items.filter(x => x.isActive && !x.shouldActivate());

  //For each item that should deactivate
  for(let item of shouldDeactivate) {
    item.deactivate();
  }
}

/**
 * Full fill all items
 * @private
 */
function _fullFillItems() {
  let elements = document.querySelectorAll(DOM_QUERYSELECTOR);

  for(let element of elements) {
    let item = this.items.filter(x => x.element == element).shift();

    if(item == null) {
      this.items.push(new ScrollTriggerItem(element));
    }
  }

}

let instance = new ScrollTrigger();

export default instance;
