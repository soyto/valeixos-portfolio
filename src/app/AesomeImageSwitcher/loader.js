import 'format-unicorn';

import SplitStrategy from './SplitStrategy';

import TwilightEffectPlugin from './plugins/TwilightEffect.plugin';
import GhostEffectPlugin from './plugins/GhostEffect.plugin';
import AccordionEffectPlugin from './plugins/AccordionEffect.plugin';

import ImageSwitcher from './ImageSwitcher';


const TRANSITION_TIMER = 10000;

let slides = [
  {
    'image': 'https://images.unsplash.com/photo-1529101091764-c3526daf38fe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2091&q=80',
    'effectPlugin': new TwilightEffectPlugin(),
    'splitStrategy': new SplitStrategy(SplitStrategy.strategies.widthAndHeight, 150, 150),
  },
  {
    'image': 'https://images.unsplash.com/photo-1496674205429-924b32acd421?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
    'effectPlugin': new GhostEffectPlugin(),
    'splitStrategy': new SplitStrategy(SplitStrategy.strategies.widthAndHeight, 100, 100),
  },
  {
    'image': 'https://images.unsplash.com/photo-1518432031352-d6fc5c10da5a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1934&q=80',
    'effectPlugin': new TwilightEffectPlugin(),
    'splitStrategy': new SplitStrategy(SplitStrategy.strategies.widthAndHeight, 100, 50),
  },
  {
    'image': 'https://images.unsplash.com/photo-1544847558-3ccacb31ee7f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
    'effectPlugin': new GhostEffectPlugin(),
    'splitStrategy': new SplitStrategy(SplitStrategy.strategies.colsAndRows, 5, 5)
  },
  {
    "image": "https://images.unsplash.com/photo-1544197150-b99a580bb7a8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80",
    "effectPlugin": new TwilightEffectPlugin(),
    "splitStrategy": new SplitStrategy(SplitStrategy.strategies.widthAndHeight, 200, 150)
  }
];



let element = document.getElementById('image-switcher');
let imageSwitcher = new ImageSwitcher(element, slides[0].image);

let _currentImageIndex = 0;


//Make an interval to do transitions
setInterval(() => {
  _currentImageIndex++;

  if(_currentImageIndex >= slides.length) { _currentImageIndex = 0; }

  let slide = slides[_currentImageIndex];

  imageSwitcher.doTransition(slide.image, slide.effectPlugin, slide.splitStrategy);
}, TRANSITION_TIMER);
