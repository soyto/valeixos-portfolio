
const CLASS_CURRENT = 'card--current';
const CLASS_NEXT = 'card--next';
const CLASS_OUT = 'card--out';
const CLASS_ANIMATE = 'animate';

/**
 * Class implemented to the card holder effect
 */
class CardHolder {

  /**
   * Constructor, receives the element
   * @param element
   */
  constructor(element) {
    this.element = element;
    this.animating = false;
  }

  /**
   * Activate a target
   * @param target
   */
  activate(target) {

    let currentActive = this.element.querySelectorAll('.' + CLASS_CURRENT);
    let currentNext = this.element.querySelectorAll('.' + CLASS_NEXT);
    let currentOut = this.element.querySelectorAll('.' + CLASS_OUT);
    let nextActive = this.element.querySelectorAll('.' + target);

    currentActive = currentActive.length ? currentActive[0] : null;
    currentNext = currentNext.length ? currentNext[0] : null;
    currentOut = currentOut.length ? currentOut[0] : null;
    nextActive = nextActive.length ? nextActive[0] : null;

    //If we are going to activate the current one, or we are not going to activate nothing
    if(!nextActive || nextActive == currentActive) { return; }

    let idx = _getNodeIndex(nextActive);
    let nextIndex = idx == this.element.children.length - 1 ? 0 : idx + 1;
    let nextNext = this.element.children[nextIndex];

    currentNext.classList.remove(CLASS_NEXT);

    currentActive.classList.remove(CLASS_CURRENT);
    currentActive.classList.add(CLASS_OUT);

    currentOut.classList.remove(CLASS_OUT);

    nextActive.classList.add(CLASS_CURRENT);

    nextNext.classList.add(CLASS_NEXT);

    nextActive.classList.add(CLASS_ANIMATE);
    nextNext.classList.add(CLASS_ANIMATE);
    currentActive.classList.add(CLASS_ANIMATE);

    /*this.animating = true;

    setTimeout(() => {
      nextActive.classList.remove(CLASS_ANIMATE);
      nextNext.classList.remove(CLASS_ANIMATE);
      currentActive.classList.remove(CLASS_ANIMATE);

      this.animating = false;
    }, 1000); */

  }
}


function _getNodeIndex(element) {
  return Array.prototype.indexOf.call(element.parentElement.children, element);
}


export default CardHolder;
